package com.example.demofilezipuploadserver

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.FileWriter
import java.text.SimpleDateFormat
import java.util.*

@SpringBootApplication
class DemoFileZipUploadServerApplication

fun main(args: Array<String>) {
	runApplication<DemoFileZipUploadServerApplication>(*args)
}

@RestController
@CrossOrigin
@RequestMapping("/upload")
class UploadController {

	companion object {
		val log = LoggerFactory.getLogger(this::class.java);
	}

	@Value(value = "\${file.upload.path}")
	private lateinit var fileUploadPath: String

	@PostMapping("/file")
	fun save(@RequestBody data: String): HttpStatus {

		val bytes = Base64.getMimeDecoder().decode(data)
		val file = File("$fileUploadPath/" + generateFileName())
		val fileWriter = FileOutputStream(file)
		fileWriter.write(bytes)
		fileWriter.flush()
		fileWriter.close()

		return HttpStatus.OK
	}

	private fun generateFileName(): String {
		val df = SimpleDateFormat("yyyyMMddHHmmss")
		return df.format(Date()) + "-" + randomString(8) + ".zip"
	}

	private fun randomString(length: Int): String {
		var rndString = ""
		val text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		for (i in 0 until length) {
			val num = Math.random() * 100
			val charIdx = num.toInt() % 25
			rndString += text.substring(charIdx, charIdx + 1)
		}
		return rndString
	}
}
